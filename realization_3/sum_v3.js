export function sum_v_3(initNumber) {
	let result = initNumber;

	const curry = number => {
		result += number;

		return curry;
	};

	curry.valueOf = curry.toString = () => result;

	return curry;
}

// valueOf и toString - это два метода, которые есть у всех объектов в JavaScript.
// Они используются для преобразования объекта в примитивное значение.
// Если метод valueOf возвращает примитивное значение, то он используется.
// Если метод valueOf не возвращает примитивное значение,
// то JavaScript вызывает метод toString, который возвращает строку.
