export function sum_v_4(initNumber) {
	let result = initNumber;

	return function next(number) {
		if (number) {
			result += number;

			return next;
		}

		return result;
	};
}
