export function sum_v_2(initNumber) {
	let result = initNumber;

	const curry = number => {
		result += number;
		console.log(result);

		return curry;
	};

	console.log(result);

	return curry;
}
